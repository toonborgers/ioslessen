//
//  Size2D.h
//  Coordinates
//
//  Created by Servaas Tilkin on 12/02/14.
//  Copyright (c) 2014 Servaas Tilkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Size2D : NSObject

@property int w;
@property int h;

+ (Size2D*) sizeWithWidth:(int)width andHeight: (int)height;

@end
