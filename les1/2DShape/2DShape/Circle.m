//
//  Circle.m
//  2DShape
//
//  Created by Tin on 19/02/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import "Circle.h"
#import <math.h>

@implementation Circle

+(Circle *)circleWithRadius:(int)radius

              andStartPoint:(Point2D *)startPoint{
    Circle* result = [[Circle alloc]init];
    
    [result setRadius:radius];
    [result setStartPoint:startPoint];
    
    return result;
}

- (double)calculateCircumference {
    return M_PI * self.radius;
}

-(double)calculateSurface{
    return self.calculateCircumference * M_PI;
}

-(NSString*) description{
    return [NSString stringWithFormat:@"%@ and radius %i", [super description], self.radius];
}

@end
