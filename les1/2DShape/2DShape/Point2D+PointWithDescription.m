//
//  Point2D+PointWithDescription.m
//  2DShape
//
//  Created by Tin on 19/02/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import "Point2D+PointWithDescription.h"

@implementation Point2D (PointWithDescription)

-(NSString*)description{
    return [NSString stringWithFormat:@"[Point at (%i, %i)]", self.x, self.y];
}

@end
