//
//  Square.h
//  2DShape
//
//  Created by Tin on 19/02/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Shape2D.h"
#import "SuperShape.h"

@interface Square : SuperShape

@property int side;

+(Square*) squareWithSide:(int)side
            andStartPoint:(Point2D*) startPoint;


@end
