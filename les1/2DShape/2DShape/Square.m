//
//  Square.m
//  2DShape
//
//  Created by Tin on 19/02/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import "Square.h"
#import "Point2D.h"

@implementation Square


+(Square *)squareWithSide:(int)side
            andStartPoint:(Point2D *)startPoint{
    
    Square* result = [[Square alloc] init];
    
    [result setSide:side];
    [result setStartPoint:startPoint];
    
    return result;
}

-(double)calculateCircumference{
    return 4*self.side;
}

-(double)calculateSurface{
    return self.side*self.side;
}

-(NSString*) description{
    return  [NSString stringWithFormat:@"%@ and side %i", [super description], self.side];
}

@end
