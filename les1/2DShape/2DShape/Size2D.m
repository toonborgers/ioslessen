//
//  Size2D.m
//  Coordinates
//
//  Created by Servaas Tilkin on 12/02/14.
//  Copyright (c) 2014 Servaas Tilkin. All rights reserved.
//

#import "Size2D.h"

@implementation Size2D

+ (Size2D*) sizeWithWidth:(int)width andHeight: (int)height
{
    Size2D *sz = [[Size2D alloc]init];
    [sz setW:width];
    [sz setH:height];
    return sz;
}

@end
