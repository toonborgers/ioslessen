//
//  Point2D.h
//  Coordinates
//
//  Created by Servaas Tilkin on 12/02/14.
//  Copyright (c) 2014 Servaas Tilkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Point2D : NSObject

@property int x;
@property int y;

+ (Point2D*) pointWithX:(int)x andY: (int)y;

@end
