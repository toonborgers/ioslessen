//
//  SuperShape.h
//  2DShape
//
//  Created by Tin on 19/02/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Shape2D.h"

@interface SuperShape : NSObject<Shape2D>

-(NSString*) description;
@end
