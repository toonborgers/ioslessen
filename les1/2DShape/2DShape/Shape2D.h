//
//  Shape2D.h
//  2DShape
//
//  Created by Tin on 19/02/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Point2D.h"

@protocol Shape2D <NSObject>

@property Point2D* startPoint;

-(double) calculateCircumference;
-(double) calculateSurface;
@end
