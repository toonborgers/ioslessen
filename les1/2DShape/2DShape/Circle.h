//
//  Circle.h
//  2DShape
//
//  Created by Tin on 19/02/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Shape2D.h"
#import "SuperShape.h"

@interface Circle : SuperShape

@property int radius;

+(Circle*) circleWithRadius:(int)radius
              andStartPoint:(Point2D*) startPoint;

@end
