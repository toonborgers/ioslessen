//
//  Oval.m
//  2DShape
//
//  Created by Tin on 19/02/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import "Oval.h"
#import <math.h>
#import "SuperShape.h"

@implementation Oval



+(Oval*) ovalWithWidth:(int)width
             andHeight:(int)height
         andStartPoint:(Point2D *)startPoint{
    
    Oval* result = [[Oval alloc]init];
    
    [result setWidth:width];
    [result setHeigth:height];
    [result setStartPoint:startPoint];
    
    return result;
}

-(double) calculateCircumference{
    return 2* M_PI * sqrt((self.width*self.width + self.heigth*self.heigth)/2);
}

-(double)calculateSurface{
    return M_PI * self.width/2 * self.heigth/2;
}

- (NSString *)description{
    return [NSString stringWithFormat:@"%@ and width %i and height %i", [super description], self.width, self.heigth];
}

@end
