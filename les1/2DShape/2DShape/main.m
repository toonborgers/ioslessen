//
//  main.m
//  2DShape
//
//  Created by Tin on 19/02/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import "Oval.h"
#import "Square.h"
#import "Circle.h"
#import "Point2D.h"
#import "Point2D+PointWithDescription.h"
#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{
    
    @autoreleasepool {
        Point2D* start = [Point2D pointWithX:5 andY:6];
        
        Oval* oval = [Oval ovalWithWidth:20 andHeight:25 andStartPoint:start];
        Square* square = [Square squareWithSide:35874 andStartPoint:start];
        Circle* circle = [Circle circleWithRadius:8468 andStartPoint:start];
        
        NSArray* container = [NSArray arrayWithObjects:oval,square,circle, nil];
        
        NSLog(@"My objects are: %@", container);
    }
    return 0;
}

