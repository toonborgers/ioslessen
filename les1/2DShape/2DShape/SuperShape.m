//
//  SuperShape.m
//  2DShape
//
//  Created by Tin on 19/02/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import "SuperShape.h"

@implementation SuperShape
@synthesize startPoint = _startPoint;

-(NSString *)description{
    return [NSString stringWithFormat:@"A shape of type %@ with start point %@, circumference %f, surface %f", NSStringFromClass(self.class), self.startPoint, self.calculateCircumference, self.calculateSurface];
}
@end
