//
//  Oval.h
//  2DShape
//
//  Created by Tin on 19/02/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Shape2D.h"
#import "SuperShape.h"

@interface Oval : SuperShape

@property int width;
@property int heigth;

+(Oval*) ovalWithWidth : (int)width
              andHeight:(int)height
          andStartPoint:(Point2D *)startPoint;

@end
