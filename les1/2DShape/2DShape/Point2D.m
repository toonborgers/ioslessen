//
//  Point2D.m
//  Coordinates
//
//  Created by Servaas Tilkin on 12/02/14.
//  Copyright (c) 2014 Servaas Tilkin. All rights reserved.
//

#import "Point2D.h"

@implementation Point2D

+ (Point2D*) pointWithX:(int)x andY: (int)y;
{
    Point2D *pt = [[Point2D alloc]init];
    
    [pt setX:x];
    [pt setY:y];
    
    return pt;
}

@end
