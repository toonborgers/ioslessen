//
//  BORViewController.h
//  FirstApp
//
//  Created by Tin on 1/03/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BORViewController : UIViewController<UITextFieldDelegate>

@property IBOutlet UITextField *todoTitleField;
- (IBAction)addToDo:(id)sender;

@end
