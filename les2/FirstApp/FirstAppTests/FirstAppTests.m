//
//  FirstAppTests.m
//  FirstAppTests
//
//  Created by Toon Borgers on 01/03/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import "FirstAppTests.h"

@implementation FirstAppTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in FirstAppTests");
}

@end
