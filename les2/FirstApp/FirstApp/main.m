//
//  main.m
//  FirstApp
//
//  Created by Toon Borgers on 01/03/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BORAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BORAppDelegate class]));
    }
}
