//
//  BORAppDelegate.h
//  FirstApp
//
//  Created by Toon Borgers on 01/03/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BORAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
