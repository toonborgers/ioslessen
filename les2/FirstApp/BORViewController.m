//
//  BORViewController.m
//  FirstApp
//
//  Created by Tin on 1/03/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import "BORViewController.h"

@interface BORViewController ()

@end

@implementation BORViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addToDo:(id)sender{
    NSLog(@"Add to do: %@", self.todoTitleField.text);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Test"
                                                    message:self.todoTitleField.text
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    self.todoTitleField.text = @"";
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(self.todoTitleField == textField){
        NSLog(@"Textfield contains: %@", self.todoTitleField.text);
        [textField resignFirstResponder];
    }
    return YES;
}
@end
