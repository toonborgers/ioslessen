//
//  BORSecondViewController.m
//  VolumeEvents
//
//  Created by Tin on 1/03/14.
//  Copyright (c) 2014 Rudy. All rights reserved.
//

#import "BORSecondViewController.h"

@interface BORSecondViewController ()

@end

@implementation BORSecondViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
