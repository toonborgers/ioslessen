//
//  BORViewController.h
//  VolumeEvents
//
//  Created by Tin on 1/03/14.
//  Copyright (c) 2014 Rudy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BORViewController : UIViewController<UITextFieldDelegate>
@property IBOutlet UISlider *ringtone;
@property IBOutlet UITextField *ringtoneText;
@property IBOutlet UIStepper *ringtoneStepper;
@property IBOutlet UISwitch *coupleSwitch;
@property IBOutlet UISlider *notification;

-(IBAction)okButtonPressed:(id)sender;
-(IBAction)resetButtonPressed:(id)sender;
-(IBAction)ringtoneVolumeChanged:(id)sender;
-(IBAction)ringtoneTextChanged:(id)sender;
-(IBAction)stepperChanged:(id)sender;
-(IBAction)toggleChanged:(id)sender;

@end
