//
//  BORViewController.m
//  VolumeEvents
//
//  Created by Tin on 1/03/14.
//  Copyright (c) 2014 Rudy. All rights reserved.
//

#import "BORViewController.h"

@interface BORViewController ()

@end

@implementation BORViewController

- (void)viewDidLoad
{
    [self.coupleSwitch setSelected:NO];
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)okButtonPressed:(id)sender{
    
}

-(void)resetButtonPressed:(id)sender{
    
}

-(void)ringtoneVolumeChanged:(id)sender{
    if(self.coupleSwitch.isOn){
        self.notification.value = self.ringtone.value;
        self.ringtoneText.text = [NSString stringWithFormat:@"%f", self.ringtone.value];
    }
}

-(void)ringtoneTextChanged:(id)sender{
    NSLog(@"Text changed: %@" , self.ringtoneText.text);
    float ringtoneTextAsFloat = [self.ringtoneText.text floatValue];
    if(ringtoneTextAsFloat < 1){
        self.ringtone.value = ringtoneTextAsFloat;
    }
}

-(void)stepperChanged:(id)sender{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(self.ringtoneText == textField){
        [textField resignFirstResponder];
    }
    return YES;
}

-(void)toggleChanged:(id)sender{
    if(self.coupleSwitch.isOn){
        self.notification.enabled = false;
    }
}

@end
