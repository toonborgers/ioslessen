//
//  BORViewController.m
//  RegisterEvents
//
//  Created by Tin on 1/03/14.
//  Copyright (c) 2014 Rudy. All rights reserved.
//

#import "BORViewController.h"

@interface BORViewController ()

@end

@implementation BORViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(self.passwordField==textField ){
        [textField resignFirstResponder];
    }
    return YES;
}

-(void)registerNew:(id)sender{
    if(self.passwordField.text.length > 5){
        [[[UIAlertView alloc ] initWithTitle:@"Success"
                                     message:@"Registration succesful!"
                                    delegate:nil
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil,
          nil]
         show];
    }
}

-(void)toggleSwitch:(id)sender{
    if(!self.toggleSave.isOn){
        self.usernameField.text = @"";
        self.passwordField.text = @"";
    }
}

@end
