//
//  BORViewController.h
//  RegisterEvents
//
//  Created by Tin on 1/03/14.
//  Copyright (c) 2014 Rudy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BORViewController : UIViewController<UITextFieldDelegate>

@property IBOutlet UITextField *usernameField;
@property IBOutlet UITextField *passwordField;
@property IBOutlet UISwitch *toggleSave;

-(IBAction)registerNew:(id)sender;

-(IBAction)toggleSwitch:(id)sender;
@end
