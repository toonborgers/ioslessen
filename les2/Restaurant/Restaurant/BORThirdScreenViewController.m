//
//  BORThirdScreenViewController.m
//  Restaurant
//
//  Created by Tin on 1/03/14.
//  Copyright (c) 2014 Rudy. All rights reserved.
//

#import "BORThirdScreenViewController.h"
#import "STPMenus.h"

@interface BORThirdScreenViewController ()

@end

@implementation BORThirdScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    int day;
    if( [self.which isEqualToString:@"ma"]){
                self.title = @"Maandag";
        day = 1;
	}else if ([self.which isEqualToString: @"di"]){
        self.title = @"Dinsdag";
        day=2;
    }else if ([self.which isEqualToString: @"wo"]){
        self.title = @"Woensdag";
        day = 3;
    }else if ([self.which isEqualToString: @"do"]){
        self.title = @"Donderdag";
        day = 4;
    }else{
        self.title = @"Vrijdag";
        day = 5;
    }
    
    NSArray *menu = [[[STPMenus alloc] init ]getMenuWithDay:day];
    NSLog(@"Menu voor dag %d: %@", day, menu);
    
    self.soep.text = [menu objectAtIndex:0];
    self.dagschotel.text = [menu objectAtIndex:1];
    self.pasta.text = [menu objectAtIndex:2];
    self.vegetarisch.text = [menu objectAtIndex:3];
    self.snack.text = [menu objectAtIndex:4];
    self.saladbar.text = [menu objectAtIndex:5];
    self.dessert.text = [menu objectAtIndex:6];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
