//
//  BORSecondScreenViewController.m
//  Restaurant
//
//  Created by Tin on 1/03/14.
//  Copyright (c) 2014 Rudy. All rights reserved.
//

#import "BORSecondScreenViewController.h"
#import "BORThirdScreenViewController.h"

@interface BORSecondScreenViewController ()

@end

@implementation BORSecondScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if( [self.which isEqualToString:@"first"]){
        NSLog(@"first");
        self.title = @"3-9 maart 2014";
	}else if ([self.which isEqualToString: @"second"]){
        NSLog(@"second");
        self.title = @"10-16 maart 2004";
    }else{
        NSLog(@"third");
        self.title = @"17-23 maart 2014";
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    BORThirdScreenViewController* controller =  (BORThirdScreenViewController*) segue.destinationViewController;
    controller.which = segue.identifier;
}

@end
