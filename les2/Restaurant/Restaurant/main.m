//
//  main.m
//  Restaurant
//
//  Created by Tin on 1/03/14.
//  Copyright (c) 2014 Rudy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BORAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BORAppDelegate class]));
    }
}
