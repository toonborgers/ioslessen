//
//  STPMenus.m
//  Restaurant
//
//  Created by Servaas Tilkin on 1/03/14.
//  Copyright (c) 2014 Servaas Tilkin. All rights reserved.
//

#import "STPMenus.h"

@implementation STPMenus


// get the full menu for one day
- (NSArray *)getMenuWithDay:(int)dayNr {
    if (dayNr >= 0 && dayNr < self.allMenus.count) {
        return [self.allMenus objectAtIndex:dayNr];
    } else {
        return nil;
    }
}

// get a single dish on a specific day
- (NSString *)getDishWithDay:(int)dayNr
                   andDishNr:(int)dishNr {
    NSArray *dayMenu = [self getMenuWithDay:dayNr];
    if (dayMenu != nil) {
        if (dishNr > 0 && dishNr < dayMenu.count){
            return [dayMenu objectAtIndex:dishNr];
        }
    }
    
    return nil;
}

// initialization method
- (id)init
{
    self = [super init];
    if (self) {
        NSLog(@"Initializing menu arrays...");
        [self initializeArrays];
    }
    return self;
}

- (void) initializeArrays{
     self.menu1 =
	[NSArray arrayWithObjects:@"Witloofsoep", @"Varkensrib met rode kool", @"Pasta met kip en champignons", @"Groentenburger",
     @"Curryworst speciaal", @"Klein/groot met vis of vlees", @"Naar keuze", nil ];
	self.menu2 =
	[NSArray arrayWithObjects:@"Wittekoolsoep", @"Rumsteak met pepersaus en gemengde salade", @"Penne met zeevruchten", @"Groentenburger",
     @"Gevulde flensjes", @"Klein/groot met vis of vlees", @"Naar keuze", nil ];
	self.menu3 =
	[NSArray arrayWithObjects:@"Tomatensoep", @"Kipfilet met appelmoes en kroketten", @"Spaghetti carbonara", @"Vegetarische wrap", @"Naar keuze",
     @"Klein/groot met vis of vlees", @"Naar keuze", nil ];
	self.menu4 =
	[NSArray arrayWithObjects:@"Preisoep", @"Wok met varkensvlees, sojascheuten en curry", @"Pasta met Couburg en kruidenkaas",
     @"Gegratineerde groentenschotel", @"Naar keuze", @"Klein/groot met vis of vlees", @"Naar keuze" , nil];
	self.menu5 =
	[NSArray arrayWithObjects:@"Witte bonensoep", @"Gratin van vis en groenten", @"Lasagne", @"Vegetarisch broodje", @"Loempia",
     @"Klein/groot met vis of vlees", @"Naar keuze" , nil];
    
    self.allMenus = [NSArray arrayWithObjects:self.menu1, self.menu2, self.menu3, self.menu4, self.menu5, nil];
}

@end
