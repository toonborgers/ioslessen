//
//  BORThirdScreenViewController.h
//  Restaurant
//
//  Created by Tin on 1/03/14.
//  Copyright (c) 2014 Rudy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BORBestelViewController.h"

@interface BORThirdScreenViewController : UIViewController<BORBestelViewControllerDelegate>

@property NSString *which;

@property IBOutlet UILabel *soep;
@property IBOutlet UILabel *dagschotel;
@property IBOutlet UILabel *pasta;
@property IBOutlet UILabel *vegetarisch;
@property IBOutlet UILabel *snack;
@property IBOutlet UILabel *saladbar;
@property IBOutlet UILabel *dessert;

@end
