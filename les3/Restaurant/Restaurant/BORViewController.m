//
//  BORViewController.m
//  Restaurant
//
//  Created by Tin on 1/03/14.
//  Copyright (c) 2014 Rudy. All rights reserved.
//

#import "BORViewController.h"
#import "BORSecondScreenViewController.h"

@interface BORViewController ()

@end

@implementation BORViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    BORSecondScreenViewController *controller = (BORSecondScreenViewController *)segue.destinationViewController;
    controller.which = segue.identifier;
}

@end
