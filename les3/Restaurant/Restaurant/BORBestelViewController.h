//
//  BORBestelViewController.h
//  Restaurant
//
//  Created by Tin on 12/03/14.
//  Copyright (c) 2014 Rudy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BORBestelViewController;

@protocol BORBestelViewControllerDelegate <NSObject>

- (void) bestelViewController: (BORBestelViewController *) contoller didFinishBestellingWithValues: (NSString *) values;

@end

@interface BORBestelViewController : UIViewController

@property IBOutlet UILabel *soep;
@property IBOutlet UILabel *dagschotel;
@property IBOutlet UILabel *pasta;
@property IBOutlet UILabel *vegetarisch;
@property IBOutlet UILabel *snack;
@property IBOutlet UILabel *saladbar;
@property IBOutlet UILabel *dessert;

@property IBOutlet UITextField *soepAantal;
@property IBOutlet UITextField *dagschotelAantal;
@property IBOutlet UITextField *pastaAantal;
@property IBOutlet UITextField *vegetarischAantal;
@property IBOutlet UITextField *snackAantal;
@property IBOutlet UITextField *saladbarAantal;
@property IBOutlet UITextField *dessertAantal;

@property IBOutlet UIStepper *soepStepper;
@property IBOutlet UIStepper *dagschotelStepper;
@property IBOutlet UIStepper *pastaStepper;
@property IBOutlet UIStepper *vegetarischStepper;
@property IBOutlet UIStepper *snackStepper;
@property IBOutlet UIStepper *saladbarStepper;
@property IBOutlet UIStepper *dessertStepper;

@property NSString *which;

@property id <BORBestelViewControllerDelegate> delegate;

-(IBAction)stepperValueChanged:(id)sender;

@end
