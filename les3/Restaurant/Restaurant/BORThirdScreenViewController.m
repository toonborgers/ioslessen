//
//  BORThirdScreenViewController.m
//  Restaurant
//
//  Created by Tin on 1/03/14.
//  Copyright (c) 2014 Rudy. All rights reserved.
//

#import "BORThirdScreenViewController.h"
#import "STPMenus.h"
#import "BORBestelViewController.h"

@interface BORThirdScreenViewController ()

@end

@implementation BORThirdScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    int day;
    if( [self.which isEqualToString:@"ma"]){
                self.title = @"Maandag";
        day = 0;
	}else if ([self.which isEqualToString: @"di"]){
        self.title = @"Dinsdag";
        day=1;
    }else if ([self.which isEqualToString: @"wo"]){
        self.title = @"Woensdag";
        day = 2;
    }else if ([self.which isEqualToString: @"do"]){
        self.title = @"Donderdag";
        day = 3;
    }else{
        self.title = @"Vrijdag";
        day = 4;
    }
    
    NSArray *menu = [[[STPMenus alloc] init ]getMenuWithDay:day];
    NSLog(@"Menu voor dag %d: %@", day, menu);
    
    self.soep.text = [menu objectAtIndex:0];
    self.dagschotel.text = [menu objectAtIndex:1];
    self.pasta.text = [menu objectAtIndex:2];
    self.vegetarisch.text = [menu objectAtIndex:3];
    self.snack.text = [menu objectAtIndex:4];
    self.saladbar.text = [menu objectAtIndex:5];
    self.dessert.text = [menu objectAtIndex:6];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    BORBestelViewController* controller =  (BORBestelViewController*) segue.destinationViewController;
    controller.which = segue.identifier;
    controller.delegate = self;
} 


-(void)bestelViewController:(BORBestelViewController *)contoller didFinishBestellingWithValues:(NSString *)values{
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Bestelling" message:values delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alertView show];
}

@end
