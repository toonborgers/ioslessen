//
//  BORBestelViewController.m
//  Restaurant
//
//  Created by Tin on 12/03/14.
//  Copyright (c) 2014 Rudy. All rights reserved.
//

#import "BORBestelViewController.h"
#import "STPMenus.h"

@implementation BORBestelViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	
    int day;
    if( [self.which isEqualToString:@"ma"]){
        self.title = @"Maandag";
        day = 0;
	}else if ([self.which isEqualToString: @"di"]){
        self.title = @"Dinsdag";
        day=1;
    }else if ([self.which isEqualToString: @"wo"]){
        self.title = @"Woensdag";
        day = 2;
    }else if ([self.which isEqualToString: @"do"]){
        self.title = @"Donderdag";
        day = 3;
    }else{
        self.title = @"Vrijdag";
        day = 4;
    }
    
    NSArray *menu = [[[STPMenus alloc] init ]getMenuWithDay:day];
    NSLog(@"Menu voor dag %d: %@", day, menu);
    
    self.soep.text = [menu objectAtIndex:0];
    self.dagschotel.text = [menu objectAtIndex:1];
    self.pasta.text = [menu objectAtIndex:2];
    self.vegetarisch.text = [menu objectAtIndex:3];
    self.snack.text = [menu objectAtIndex:4];
    self.saladbar.text = [menu objectAtIndex:5];
    self.dessert.text = [menu objectAtIndex:6];
}

-(void)viewWillDisappear:(BOOL)animated{
    if(self.delegate != nil){
        NSString* resultString = [NSString stringWithFormat:@"%@: %@\n%@: %@\n%@: %@\n%@: %@\n%@: %@\n%@: %@\n%@: %@",
                                  self.soep.text, self.soepAantal.text,
                                  self.dagschotel.text, self.dagschotelAantal.text,
                                  self.pasta.text, self.pastaAantal.text,
                                  self.vegetarisch.text, self.vegetarischAantal.text,
                                  self.snack.text, self.snackAantal.text,
                                  self.saladbar.text, self.saladbarAantal.text,
                                  self.dessert.text, self.dessertAantal.text];
        
        [self.delegate bestelViewController:self didFinishBestellingWithValues:resultString];
    }
}

-(void)stepperValueChanged:(id)sender{
    UIStepper* theStepper = (UIStepper*)sender;
    int intVal = theStepper.value;
    NSString* stringVal =  [NSString stringWithFormat:@"%d",intVal];
    
    if ([theStepper isEqual:self.soepStepper]) {
        self.soepAantal.text = stringVal;
        
    }else if ([theStepper isEqual:self.dagschotelStepper]){
        self.dagschotelAantal.text = stringVal;
        
    }else if ([theStepper isEqual:self.pastaStepper]){
        self.pastaAantal.text = stringVal;
        
    }else if ([theStepper isEqual:self.vegetarischStepper]){
        self.vegetarischAantal.text = stringVal;
        
    }else if ([theStepper isEqual:self.snackStepper]){
        self.snackAantal.text = stringVal;
        
    }else if ([theStepper isEqual:self.saladbarStepper]){
        self.saladbarAantal.text = stringVal;
        
    }else if ([theStepper isEqual:self.dessertStepper]){
        self.dessertAantal.text = stringVal;
    }
}

@end
