//
//  STPMenus.h
//  Restaurant
//
//  Created by Servaas Tilkin on 1/03/14.
//  Copyright (c) 2014 Servaas Tilkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface STPMenus : NSObject

@property NSArray *allMenus;
@property NSArray *menu1;
@property NSArray *menu2;
@property NSArray *menu3;
@property NSArray *menu4;
@property NSArray *menu5;

- (NSArray*) getMenuWithDay:(int) dayNr;
- (NSString*) getDishWithDay: (int) dayNr
                   andDishNr: (int) dishNr;

@end
