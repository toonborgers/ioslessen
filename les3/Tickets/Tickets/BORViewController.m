//
//  BORViewController.m
//  Tickets
//
//  Created by Tin on 12/03/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import "BORViewController.h"
#import "BORTravelInformationViewController.h"
#import "BORTravelDatesViewController.h"

@interface BORViewController ()

@end

@implementation BORViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.ticketConfiguration = [TicketConfiguration withDefaults];
    [self fillLabelsWithConfiguration];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) fillLabelsWithConfiguration{
    self.departure.text = self.ticketConfiguration.departure;
    self.destination.text = self.ticketConfiguration.destination;
    self.adults.text = [NSString stringWithFormat:@"%d", self.ticketConfiguration.adults];
    self.children.text = [NSString stringWithFormat:@"%d", self.ticketConfiguration.children];
    self.extraLuggage.text = [NSString stringWithFormat:@"%@", self.ticketConfiguration.extraLuggage? @"YES":@"NO"];
    self.inflightMeal.text =  [NSString stringWithFormat:@"%@", self.ticketConfiguration.inflightMeal? @"YES":@"NO"];
    self.departureMonth.text = [self.ticketConfiguration.departureDate description];
    self.returnMonth.text = [self.ticketConfiguration.returnDate description];
    self.price.text = [NSString stringWithFormat:@"€%d", self.ticketConfiguration.price];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"toEdit"]){
        NSLog(@"%@", segue.destinationViewController);
        UINavigationController* navController = (UINavigationController*)segue.destinationViewController;
        BORTravelInformationViewController* destController = (BORTravelInformationViewController*)[navController.childViewControllers objectAtIndex:0];
        destController.ticketConfiguration = self.ticketConfiguration;
    }
}

-(void)unwindToStart:(UIStoryboardSegue *)segue{
    NSLog(@"Unwind to start");
    if([segue.identifier isEqualToString:@"unwindToStart"]){
        BORTravelDatesViewController* source = (BORTravelDatesViewController*) segue.sourceViewController;
        self.ticketConfiguration = source.ticketConfiguration;
        [self fillLabelsWithConfiguration];
    }
    
}

@end
