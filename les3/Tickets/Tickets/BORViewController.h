//
//  BORViewController.h
//  Tickets
//
//  Created by Tin on 12/03/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TicketConfiguration.h"

@interface BORViewController : UIViewController

@property IBOutlet UILabel *departure;
@property IBOutlet UILabel *destination;
@property IBOutlet UILabel *adults;
@property IBOutlet UILabel *children;
@property IBOutlet UILabel *extraLuggage;
@property IBOutlet UILabel *inflightMeal;
@property IBOutlet UILabel *departureMonth;
@property IBOutlet UILabel *returnMonth;
@property IBOutlet UILabel *price;

@property TicketConfiguration* ticketConfiguration;

-(IBAction)unwindToStart:(UIStoryboardSegue*)segue;

@end
