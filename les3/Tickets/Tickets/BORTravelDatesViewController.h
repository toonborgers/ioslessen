//
//  BORTravelDatesViewController.h
//  Tickets
//
//  Created by Tin on 12/03/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TicketConfiguration.h"


@class BORTravelDatesViewController;

@protocol BORTravelDatesViewControllerDelegate <NSObject>

- (void) travelDatesViewController: (BORTravelDatesViewController *) contoller didFinishBestellingWithValues: (TicketConfiguration *) configuration;

@end

@interface BORTravelDatesViewController : UIViewController

@property TicketConfiguration* ticketConfiguration;

@property IBOutlet UISwitch* withReturnFlight;
@property IBOutlet UIDatePicker* departureDatePicker;
@property IBOutlet UIDatePicker* returnDatePicker;

@property id<BORTravelDatesViewControllerDelegate> delegate;

-(IBAction)returnFlightChanged:(id)sender;
-(IBAction)departureDateChanged:(id)sender;
-(IBAction)returnDateChanged:(id)sender;

@end
