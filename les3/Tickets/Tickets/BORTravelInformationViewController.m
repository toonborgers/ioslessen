//
//  BORTravelInformationViewController.m
//  Tickets
//
//  Created by Tin on 12/03/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import "BORTravelInformationViewController.h"
#import "BORTravelDatesViewController.h"

@interface BORTravelInformationViewController ()

@end

@implementation BORTravelInformationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.departure.text = self.ticketConfiguration.departure;
    self.destination.text = self.ticketConfiguration.destination;
    self.adultsSlider.value = self.ticketConfiguration.adults;
    self.adultsText.text = [NSString stringWithFormat:@"%d",self.ticketConfiguration.adults];
    self.childrenSlider.value = self.ticketConfiguration.children;
    self.childrenText.text = [NSString stringWithFormat:@"%d",self.ticketConfiguration.children];
    [self.extraLuggage setOn:self.ticketConfiguration.extraLuggage];
    [self.inflightMeal setOn:self.ticketConfiguration.inflightMeal];    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)sliderChanged:(id)sender{
    UISlider* slider = (UISlider*)sender;
    float floatVal = slider.value;
    int intVal = (int)floatVal;
    NSString* stringVal = [NSString stringWithFormat:@"%d", intVal];
    
    if([self.adultsSlider isEqual:slider]){
        self.adultsText.text = stringVal;
    }else if ([self.childrenSlider isEqual:slider]){
        self.childrenText.text = stringVal;
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"toTravelDates"]){
        NSLog(@"dest: %@", segue.destinationViewController);
        BORTravelDatesViewController* destController = (BORTravelDatesViewController*)segue.destinationViewController;
        self.ticketConfiguration.departure = self.departure.text;
        self.ticketConfiguration.destination = self.destination.text;
        self.ticketConfiguration.adults = [self.adultsText.text intValue];
        self.ticketConfiguration.children = [self.childrenText.text intValue];
        self.ticketConfiguration.extraLuggage = [self.extraLuggage isOn];
        self.ticketConfiguration.inflightMeal = [self.inflightMeal isOn];
        
        destController.ticketConfiguration = self.ticketConfiguration;
        destController.delegate = self;
    }
}

-(void)travelDatesViewController:(BORTravelDatesViewController *)contoller didFinishBestellingWithValues:(TicketConfiguration *)configuration{
    NSLog(@"Did finish bestelling");
    self.ticketConfiguration = configuration;
    NSLog(@"Selected departure date: %@", [self.ticketConfiguration.departureDate description]);
}

@end
