//
//  TicketConfiguration.h
//  Tickets
//
//  Created by Tin on 12/03/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TicketConfiguration : NSObject

@property NSString* departure;
@property NSString* destination;
@property int adults;
@property int children;
@property BOOL extraLuggage;
@property BOOL inflightMeal;
@property BOOL withReturnFlight;
@property NSDate* departureDate;
@property NSDate* returnDate;
@property int price;

+(TicketConfiguration*) withDefaults;

@end
