//
//  BORTravelInformationViewController.h
//  Tickets
//
//  Created by Tin on 12/03/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TicketConfiguration.h"
#import "BORTravelDatesViewController.h"

@interface BORTravelInformationViewController : UIViewController<BORTravelDatesViewControllerDelegate>

@property TicketConfiguration* ticketConfiguration;

@property IBOutlet UITextField* departure;
@property IBOutlet UITextField* destination;
@property IBOutlet UISlider* adultsSlider;
@property IBOutlet UITextField* adultsText;
@property IBOutlet UISlider* childrenSlider;
@property IBOutlet UITextField* childrenText;
@property IBOutlet UISwitch* extraLuggage;
@property IBOutlet UISwitch* inflightMeal;

-(IBAction)sliderChanged:(id)sender;

@end
