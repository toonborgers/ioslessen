//
//  BORTravelDatesViewController.m
//  Tickets
//
//  Created by Tin on 12/03/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import "BORTravelDatesViewController.h"

@interface BORTravelDatesViewController ()

@end

@implementation BORTravelDatesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.departureDatePicker.date = self.ticketConfiguration.departureDate;
	[self.withReturnFlight setOn:self.ticketConfiguration.withReturnFlight];
    self.returnDatePicker.hidden = !self.ticketConfiguration.withReturnFlight;
    self.returnDatePicker.date = self.ticketConfiguration.returnDate;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)returnFlightChanged:(id)sender{
    self.returnDatePicker.hidden = ![self.withReturnFlight isOn];
}

-(void)departureDateChanged:(id)sender{
    self.ticketConfiguration.departureDate = self.departureDatePicker.date;
    NSLog(@"Selected departure date: %@", [self.ticketConfiguration.departureDate description]);
}

-(void)returnDateChanged:(id)sender{
    self.ticketConfiguration.returnDate = self.returnDatePicker.date;
    NSLog(@"Selected return date: %@", [self.ticketConfiguration.returnDate description]);
}

-(void)viewWillDisappear:(BOOL)animated{
    if(self.delegate != nil){
       [self.delegate travelDatesViewController:self didFinishBestellingWithValues:self.ticketConfiguration];
    }
}

@end
