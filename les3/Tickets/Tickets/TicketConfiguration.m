//
//  TicketConfiguration.m
//  Tickets
//
//  Created by Tin on 12/03/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import "TicketConfiguration.h"

@implementation TicketConfiguration

+(TicketConfiguration *)withDefaults{
    TicketConfiguration* result = [[TicketConfiguration alloc]init];
    
    result.departure = @"Brussels";
    result.destination = @"Madrid";
    result.adults = 1;
    result.children = 1;
    result.extraLuggage = NO;
    result.inflightMeal = YES;
    result.withReturnFlight = YES;
    result.departureDate = [NSDate date];
    result.returnDate = [NSDate date];
    result.price = 123;
    
    return result;
}
@end
