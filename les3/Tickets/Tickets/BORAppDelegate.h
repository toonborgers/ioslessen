//
//  BORAppDelegate.h
//  Tickets
//
//  Created by Tin on 12/03/14.
//  Copyright (c) 2014 Toon Borgers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BORAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
