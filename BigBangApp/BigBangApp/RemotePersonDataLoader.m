//
//  RemotePersonDataLoader.m
//  BigBangApp
//
//  Created by Tin on 23/04/14.
//  Copyright (c) 2014 PXL. All rights reserved.
//

#import "RemotePersonDataLoader.h"
#import "PersonListConnectionDelegate.h"

@implementation RemotePersonDataLoader
-(void)fetchPersonData:(CompletionBlock)completion{
    NSURL *url = [NSURL URLWithString:@"http://idamf-restdemo.herokuapp.com/app/persons.json"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    PersonListConnectionDelegate *delegate = [[PersonListConnectionDelegate alloc]initWithCompletion:^(id result, NSError *error) {
        NSLog(@"Result was %@", result);
        NSLog(@"Error was %@", error);
        completion(result, error);
        
    }];
    
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:delegate];
    [connection start];
}
@end
