//
//  RemotePersonDataLoader.h
//  BigBangApp
//
//  Created by Tin on 23/04/14.
//  Copyright (c) 2014 PXL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PersonDataLoader.h"

@interface RemotePersonDataLoader : NSObject<PersonDataLoader>

@end
